# -*- coding: UTF-8 -*-
from app import app
from flask import render_template, jsonify
from datetime import datetime
import os
import json

@app.route("/")
@app.route("/index")
def index():
  cd = datetime.now()
  EOFFlag = False
  latest_coords = []
  coords = []
  poses = []
  latlong_file = '%d-%d-%d_latlong.txt' %(cd.day, cd.month, cd.year)
  pose_file = '%d-%d-%d_pose.txt' %(cd.day, cd.month, cd.year)
  rute = os.path.join(app.root_path, latlong_file)
  
  #------------------------------latlong section------------------------------
  if(os.path.isfile(rute)):
    f = open(rute,'r')
    
    #--------------------------file reading module----------------------------
    while EOFFlag == False:
      coords_str = f.readline()
      if coords_str == '':
        #found EOF
        EOFFlag = True
      else:
        latest_latlong = coords_str
        coords_str = coords_str.split()
        coords_to_json = {'lat': float(coords_str[0]), 'lng': float(coords_str[1])}
        coords.append(coords_to_json)

    latest_coords = latest_latlong.split()
    latest_coords = {'lat': float(latest_coords[0]), 'lng': float(latest_coords[1])}
    latest_coords = json.dumps(latest_coords, ensure_ascii=False)
    coords = json.dumps(coords, ensure_ascii=False)
    f.close()
  else:
    print latlong_file+' not found, defaulting to Sydney\n'
    latest_coords = {'lat': -33.8688, 'lng': 151.2093}
    latest_coords = json.dumps(latest_coords, ensure_ascii=False)
    #-------------------------------------------------------------------------
  #---------------------------------------------------------------------------
  
  #------------------------------pose section-----------------------------------
  EOFFlag = False
  rute = os.path.join(app.root_path, pose_file)
  if(os.path.isfile(rute)):
    f = open(rute,'r')
    
    #--------------------------file reading module----------------------------
    while EOFFlag == False:
      read_str = f.readline()
      if read_str == '':
        #found EOF
        EOFFlag = True
      else:
        latest_pose = read_str
        pose_str = read_str.split()
        pose_to_json = {'roll': float(pose_str[0]), 'pitch': float(pose_str[1]), 'yaw': float(pose_str[2])}
        poses.append(pose_to_json)

    poses = json.dumps(poses, ensure_ascii=False)
    f.close()
  else:
    print pose_file+' not found!\n'
  #-----------------------------------------------------------------------------
  
  return render_template('index.html', latest_coords=latest_coords, coords=coords, poses=poses)
  
@app.route("/updatePoses", methods=['GET'])
def updatePoses():
  cd = datetime.now()
  EOFFlag = False
  poses = []
  pose_file = '%d-%d-%d_pose.txt' %(cd.day, cd.month, cd.year)
  #------------------------------pose section-----------------------------------
  rute = os.path.join(app.root_path, pose_file)
  if(os.path.isfile(rute)):
    f = open(rute,'r')
    
    #--------------------------file reading module----------------------------
    while EOFFlag == False:
      read_str = f.readline()
      if read_str == '':
        #found EOF
        EOFFlag = True
      else:
        latest_pose = read_str
        pose_str = read_str.split()
        pose_to_json = {'roll': float(pose_str[0]), 'pitch': float(pose_str[1]), 'yaw': float(pose_str[2])}
        poses.append(pose_to_json)

    f.close()
  else:
    print pose_file+' not found!\n'
  #-----------------------------------------------------------------------------
  return jsonify(poses)
  
@app.route("/updateCoords", methods=["GET"])
def updateCoords():
  cd = datetime.now()
  EOFFlag = False
  coords = []
  latlong_file = '%d-%d-%d_latlong.txt' %(cd.day, cd.month, cd.year)
  rute = os.path.join(app.root_path, latlong_file)
  
  #------------------------------latlong section------------------------------
  if(os.path.isfile(rute)):
    f = open(rute,'r')
    
    #--------------------------file reading module----------------------------
    while EOFFlag == False:
      coords_str = f.readline()
      if coords_str == '':
        #found EOF
        EOFFlag = True
      else:
        latest_latlong = coords_str
        coords_str = coords_str.split()
        coords_to_json = {'lat': float(coords_str[0]), 'lng': float(coords_str[1])}
        coords.append(coords_to_json)

    latest_coords = latest_latlong.split()
    latest_coords = {'lat': float(latest_coords[0]), 'lng': float(latest_coords[1])}
    f.close()
  else:
    print latlong_file+' not found, defaulting to Sydney\n'
    latest_coords = {'lat': -33.8688, 'lng': 151.2093}
    #-------------------------------------------------------------------------
  #---------------------------------------------------------------------------
  
  return jsonify(latest_coords, coords)
  
  
@app.route("/stop")
def stop():
  stop_file = open('stop.txt','w')
  stop_file.write('STOP\n')
  stop_file.close()
  return render_template('stop.html')

