#!flask/bin/python
from flask import Flask
app = Flask(__name__)

@app.route("/")
def hello():
  return (
    "<h1>I will try and open a file and display it for the whole world to see</h1>"
    )

if __name__ == "__main__":
  app.run()
