#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <Winsock2.h>
#include <unistd.h>

#define PORTNUM "40000"

int main(int argc, char **argv)
{
  int s;
  int sock_fd = socket(AF_INET, SOCK_STREAM, 0);
  int n = 0;

  struct addrinfo hints, *result;
  memset(&hints, 0, sizeof(struct addrinfo));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;

  s = getaddrinfo("www.robotics.unsw.edu.au", PORTNUM, &hints, &result);
  if(s != 0) {
    fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
    exit(1);
  }

  connect(sock_fd, result->ai_addr, result->ai_addrlen);
  printf("try sending multiple texts\n");
  char buffer[256];
  // for(n = 0; n < 6; n++) {
    //scanf("%s", buffer);
    while(fgets(buffer,256,stdin) !=NULL ){ // or connection terminated
      write(sock_fd, buffer, strlen(buffer));
      printf("sent: %s\n", buffer);
    // buffer[50]="";
  }
  return 0;
}
