#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
//if client is running on Windows
#ifdef __WIN32__

#include <Winsock2.h>

#else

#include <sys/socket.h>
#include <netdb.h>
// #include <netinet/in.h>

#endif

#define PORTNUM "40000"
#define DATA_LENGTH "5"
#define NON_NULL "1"

int sendString(char *input);

int main(int argc, char **argv)
{
  char data[512];
  FILE *file;
  //open file here for sensor data
  // file = fopen("A2014_05_28_11_00_GPSData.m", "rt");
  file = fopen("A2014_05_23_13_44_GPSData.m", "rt"); // can be used for presentation
  // file = fopen("PathiNewLot10Runs100m.m", "rt");
  if(file == NULL) {
    fprintf(stderr,"File is empty/not found, terminating program.");
    exit(1);
  }
  while(fgets(data, sizeof(data), file)) {
    sendString(data);
    // usleep(500000);//500 miliseconds of sleep
    usleep(20000);//20 miliseconds of sleep
  }

  printf("All data read, terminating program.\n");
  return 0;
}

int sendString(char *input)
{

  int s;
  int sock_fd = socket(AF_INET, SOCK_STREAM, 0);
  struct addrinfo hints, *result;
  memset(&hints, 0, sizeof(struct addrinfo));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;

  // s = getaddrinfo("www.robotics.unsw.edu.au", PORTNUM, &hints, &result);
  s = getaddrinfo("127.0.0.1", PORTNUM, &hints, &result);//easy debugging purposes
  if(s != 0) {
    fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
    exit(1);
  }

  connect(sock_fd, result->ai_addr, result->ai_addrlen);
  if( send(sock_fd, input, strlen(input), 0) < 0) {
        puts("Send failed");
    }

  close(sock_fd);
  return 0;
}
