#!/usr/bin/python

import socket
import utm #utm.to_latlon(east,north,56,'H')
from datetime import datetime

def processData(data):
  if ';' in data:
    data = data.replace(';','')
  return data


def convertUTMtoLatlong(data):
  #Sydney's location code is 56H
  coords = data.split()
  latlong = utm.to_latlon(float(coords[7]),float(coords[6]),56,'H')
  # latlong = utm.to_latlon(float(coords[0]),float(coords[1]),56,'H')
  return latlong


s = socket.socket(
    socket.AF_INET, socket.SOCK_STREAM)
# host = 'www.robotics.unsw.edu.au'
host = ''#easy debugging purposes
port = 40000
s.bind((host, port))
s.listen(5)
done = 0
cd = datetime.now()
cur_date = '%d-%d-%d.txt' %(cd.day, cd.month, cd.year)
latlong_date = './flaskserver/app/%d-%d-%d_latlong.txt' %(cd.day, cd.month, cd.year)
pose_date = './flaskserver/app/%d-%d-%d_pose.txt' %(cd.day, cd.month, cd.year)
f = open(cur_date, 'a')
f.close()
print 'Server opened on port 40000, waiting for connection'

while done == 0:
    #this would require it to reconnect for every data sent, to be used with sendstring
    (client, address) = s.accept()
    f = open(cur_date, 'a')
    text = client.recv(1024).decode()
    #removes whatever value is in latestData
    l = open('latestData.txt', 'w')
    l.write("")
    l.close()
    #
    l = open('latestData.txt', 'a')
    latlongfile = open(latlong_date,'a')
    posefile = open(pose_date, 'a')
    if '%' not in text and '[' not in text and ']' not in text:
      text = processData(text)
      split_text = text.split()
      latlong = convertUTMtoLatlong(text)
      latlongstring = str(latlong[0])+'\t'+str(latlong[1])+'\n'
      posestring = split_text[20]+'\t'+split_text[21]+'\t'+split_text[22]+'\n'
      l.write(text)
      f.write(text)
      latlongfile.write(latlongstring)
      posefile.write(posestring)
    else:
      print 'Invalid data received'
    l.close()
    f.close()
    latlongfile.close()
    posefile.close()
    #done to handle day changes-------------------------------------------
    cur_date = '%d-%d-%d.txt' %(cd.day, cd.month, cd.year)
    latlong_date = './flaskserver/app/%d-%d-%d_latlong.txt' %(cd.day, cd.month, cd.year)
    pose_date = './flaskserver/app/%d-%d-%d_pose.txt' %(cd.day, cd.month, cd.year)
    #----------------------------------------------------------------------
    s.shutdown(1)
    print 'Terminating connection, waiting for another connection'
